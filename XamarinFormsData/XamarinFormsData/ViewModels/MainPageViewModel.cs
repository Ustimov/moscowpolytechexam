﻿using Xamarin.Forms;

namespace XamarinFormsData.ViewModels
{
    public class MainPageViewModel
    {
        private INavigation _navigation;

        public MainPageViewModel(INavigation navigation)
        {
            _navigation = navigation;
        }
    }
}